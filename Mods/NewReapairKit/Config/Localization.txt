Key,russian

RepairKitPrimitiveGun,Ремонтный набор трубопушек

SimpleRepairKit,Простой Ремонтный набор

RepairKitArmor,Ремонтный набор для Брони

RepairKitAutomaticRiffle,Ремонтный набор для Автоматов

RepairKitRiffle,Ремонтный набор для Винтовки

RepairKitShotgun,Ремонтный набор для Дробовика

RepairKitPistols,Ремонтный набор для Пистолета

RepairKitRocketLauncher,Ремонтный набор для Ракетомета

RepairKitMechanical,Ремонтный набор для Бензоинструментов

ToolsForTransport,Инструменты для транспорта

CosmeticRepairKitForTransport,Косметический набор для транспорта

RepairKitShok,Ремонтный набор для Жезла

RepairKitRobo,Ремонтный набор Роботехника

RepairKitCrossbow,Ремонтный набор для Арбалета