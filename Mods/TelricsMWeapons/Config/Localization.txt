Key,english
------------------Colors Orange [e7a729]  Blue [2b87eb]----------------
----Schematics----
weaponMetalPipeSchematic,"Schematic: Rusty Metal Pipe"
weaponGuitarSchematic,"Schematic: Acoustic Guitar"
weaponGolfClubSchematic,"Schematic: Golf Club"
weaponCrowbarSchematic,"Schematic: Crowbar"

----Weapons----
weaponMetalPipe,"Rusty Metal Pipe"
weaponMetalPipeDesc,"A rusty pipe is useful for bashing brains in. Just don't cut yourself on the rusty bits!\n\nThis is a blunt weapon, using Pummel Pete perks."
weaponGuitar,"Acoustic Guitar"
weaponGuitarDesc,"No longer able to play music, this instrument can cave in a skull quite effectively.\n\nThis is a blunt weapon, using Pummel Pete perks."
weaponGolfClub,"Golf Club"
weaponGolfClubDesc,"Get a birdie by popping off the head of some zombies with this metal weapon.\n\nThis is a blunt weapon, using Pummel Pete perks."
weaponCrowbar,"Crowbar"
weaponCrowbarDesc,"This weapon deals 2x damage to wooden structures. It's also good for slaying zombies.\n\nThis is a blunt weapon, using Pummel Pete perks."