Key,Source,Context,Changes,English,Russian

AdvancedchemistryStation,blocks,Workstation,,Advanced Chemistry Station,Продвинутая Химическая станция
AdvancedchemistryStationSchematic,blocks,Workstation,,,Advanced Chemistry Station Schematic,,
AdvancedchemistryStationDesc,blocks,Workstation,,The Advanced Chemistry Station is an upgraded version of the regular Chemistry Station. It does not require any fuel and does not attract screamers.,Усовершенствованная химическая станция - открывает возможности, недоступные обычной химической станции
